[remark]:<class>(center, middle)
# Java Reflection 

[remark]:<slide>(new)
## Uvod
Reflexce je jednou z významných vlastností programovacího jazyce Java. 

Umožňuje provést Java program pro zkoumání nebo "introspect" na sebe, a manipulovat s vnitřními vlastnostmi programu. 

Například může získat jména všech atributů a metod třídy a zobrazit je.

Schopnost zkoumat a manipulovat s třídou uvnitř sebe nemusí jako významaná vlastnost, ale v jiných programovacích jazycích tato vlastnost jednoduše neexistuje.

Jedno hmatatelné využití reflexe je v jazyce JavaBeans, kde mohou být softwarové komponenty manipulovány vizuálně prostřednictvím nástroje Builder.

[remark]:<slide>(new)
### Jednoduchý příklad
Chcete-li zjistit, jak funguje reflexe, zvažte tento jednoduchý příklad:

```java
import java.lang.reflect.*;

public class DumpMethods {
  public static void main(String args[]) {
     try {
        Class c = Class.forName("java.util.Stack");
        Method m[] = c.getDeclaredMethods();
        for (int i = 0; i < m.length; i++)
        System.out.println(m[i].toString());
     } catch (Throwable e) {
        System.err.println(e);
     }
  }
}
```

[remark]:<slide>(new)
#### Jednoduchý příklad - výstup

Výpsání všech metod rozhraní `java.util.Stack`.
```
public synchronized java.lang.Object java.util.Stack.pop()
public java.lang.Object java.util.Stack.push(java.lang.Object)
public synchronized java.lang.Object java.util.Stack.peek()
public boolean java.util.Stack.empty()
public synchronized int java.util.Stack.search(java.lang.Object)
```

[remark]:<slide>(wait)
Tento program načte zadanou třídu pomocí třídy Class.forName.

Volá `getDeclaredMethods` pro načtení seznamu metod definovaných ve třídě.
 
Třída `java.lang.reflect.Method` je třída reprezentující metodu jedné třídy.


[remark]:<slide>(new)
## Použití reflexe

Používání v programech, které vyžadují schopnost zkoumat nebo měnit běhové chování aplikací běžících na virtuálním stroji Java. 

Toto je poměrně pokročilá funkce a měla by být používána pouze vývojáři, kteří mají silné pochopení základů jazyka. 

S ohledem na tuto výhradu je reflexe výkonnou technikou a umožňuje aplikacím provádět operace, které by jinak nebyly možné.

[remark]:<slide>(wait)
### Rozšiřitelnost
Aplikace může používat externí uživatelem definované třídy.
 
Vytvářením instancí objektů s nvovou funkcionalitou pomocí jejich plně kvalifikovaných jmen.

[remark]:<slide>(new)
### Prohlížeče třídy a prostředí pro vývoj viditelnosti
Prohlížeč třídy musí být schopen vyčíslit členy tříd. 

Prostřednictvím reflexem můřeme uživatleli vypsat seznam vlastností objeků.

[remark]:<slide>(wait)
### Debuggers a testovací nástroje
Při debuggingu lze přisoupit k soukromým atributům objektů. 

Při testování můžeme zjistit hodnoty privátních aributů ve třídě.

[remark]:<slide>(new)
## Nevýhody reflexe
Reflexe je silný nátroj, ale měl by se používat jen zřítka. 

Pokud je možné provádět operaci bez použití reflexe, mělo by se jí vyhnout. 

[remark]:<slide>(wait)
### Výkonnost režie
Vytuži reflexe znemožňuje používání některých optimalizací JVM.

[remark]:<slide>(wait)
### Bezpečnostní omezení
Reflexe může obcházet některé metody které se souští v rámci správce zabezpečení. 

To je důležité pro kód, který musí běžet v omezeném bezpečnostním kontextu, například v Applet.

[remark]:<slide>(new)
### Expozice internálů
Vzhledem k tomu, že reflexe umožňují kódům provádět operace, které by byly nezákonné v nereflexním kódu:
- přístupu k soukromým polím a metodám,
 
může použití reflexe vést k neočekávaným vedlejším účinkům, které mohou způsobit, že kód bude nefunkční a může zničit přenositelnost. 

Reflexní kód nedodržuej abstrakce, a proto může působit problémy přechodech na novější verze platformy.

[remark]:<slide>(new)
## Nastavení pro použití reflexe

Třídy reflexe, naleznete v balíku `java.lang.reflect`. 

Existují tři kroky, které je třeba dodržet při používání těchto tříd. 

[remark]:<slide>(wait)
### 1. získat objekt java.lang.Class
 
`java.lang.Class` se používá k reprezentování tříd a rozhraní v běžícím Java programu.

Jedním ze způsobů získání objektu třídy je říci:

```java
Class c = Class.forName("java.lang.String"); 
```

nebo použít známou třídu přímo:
```java
Class c = int.class; 
Class c = Integer.TYPE; 
```

[remark]:<slide>(new)
### 2. volání metody `getDeclaredMethods`

Druhým krokem je zavolat metodu, jako je getDeclaredMethods pro získání všech metod deklarovaných třídou.

Jakmile je seznam metod k dispozici, můžeme přisoupit ke třetímu kroku.

[remark]:<slide>(wait)
### 3. Použití reflexe API
 
Třetí krok je použití reflexe API. 

Například sekvence:

```java
Class c = Class.forName ("java.lang.String"); 
Metoda m[] = c.getDeclaredMethods (); 
System.out.println (m[0].toString());
```

zobrazí první metody deklarované v řetězci.

[remark]:<slide>(new)
## Testování instance třídy

Metoda `Class.isInstance` může být například použita místo operátoru instanceof:

```java
class A {}

public class instance1 {
  public static void main(String args[]) {
     try {
        Class cls = Class.forName("A");
        boolean b1 = cls.isInstance(new Integer(37));
        System.out.println(b1);
        boolean b2 = cls.isInstance(new A());
        System.out.println(b2);
     } catch (Throwable e) {
        System.err.println(e);
     }
  }
}
```

[remark]:<slide>(wait)
- V tomto příkladu je vytvořen objekt Třídy pro `A`.
- Pak je testování celé číslo (37),
- A Další instance třídy `A`.

[remark]:<slide>(new)
## Práce s metodami třídy

Práce s metodami nejzákladnějších způsobů využití reflexe.
 
Cílem je zjistit jeké metody třída obsahuje.

Někdy jak je programově použít.

```java
import java.lang.reflect.*;

public class method1 {
    
  private int f1(Object p, int x) throws NullPointerException {
     if (p == null)
        throw new NullPointerException();
     return x;
  }
}  
```

[remark]:<slide>(new)
```java
public static void main(String args[]){
  try {
    Class cls = Class.forName("method1");
    Method methlist[] = cls.getDeclaredMethods();
    for (int i = 0; i < methlist.length; i++) {  
       Method m = methlist[i];
       System.out.println("name = " + m.getName());
       System.out.println("decl class = " + m.getDeclaringClass());
       Class pvec[] = m.getParameterTypes();
       for (int j = 0; j < pvec.length; j++)
          System.out.println("param #" + j + " " + pvec[j]);
       Class evec[] = m.getExceptionTypes();
       for (int j = 0; j < evec.length; j++)
          System.out.println("exc #" + j + " " + evec[j]);
       System.out.println("return type = " + m.getReturnType());
       System.out.println("-----");
    }
  } catch (Throwable e) {
    System.err.println(e);
  }
}

```

[remark]:<slide>(new)
#### Rozbor
- Nejprve získáme Class objekt pro `method1`.
- potom volá `getDeclaredMethods` se provede načtení seznamu metod
- Tyto metody mohou být: veřejné, chráněné, balíkové a soukromé metody. 
- Pokud používáte `getMethods` můžete také získat informace o zděděných metodách.
- Jakmile máme objekt reprezentující metodu, zísáme:
  - informací o typech parametrů
  - typech výjimek
  - návratovém typu
  - annotaci. 
Každý z těchto typů, je opět reprezentován objektem `Class`.

[remark]:<slide>(wait)
#### Výstupem programu je:

```
name = f1
decl class = class method1
param #0 class java.lang.Object
param #1 int
exc #0 class java.lang.NullPointerException
return type = int
-----
name = main
decl class = class method1
param #0 class [Ljava.lang.String;
return type = void
-----
```

[remark]:<slide>(new)
## Práce s konstruktory

Podobný přístup se používá k zjištění konstruktérů třídy. 

Například:

```java
import java.lang.reflect.*;
    
public class constructor1 {
  public constructor1() {}
    
  protected constructor1(int i, double d) {}
}
  
```

[remark]:<slide>(new)
```java    
public static void main(String args[]) {
  try {
    Class cls = Class.forName("constructor1");

    Constructor ctorlist[] = cls.getDeclaredConstructors();
    for (int i = 0; i < ctorlist.length; i++) {
       Constructor ct = ctorlist[i];
       System.out.println("name = " + ct.getName());
       System.out.println("decl class = " + ct.getDeclaringClass());
       Class pvec[] = ct.getParameterTypes();
       for (int j = 0; j < pvec.length; j++)
          System.out.println("param #" + j + " " + pvec[j]);
       Class evec[] = ct.getExceptionTypes();
       for (int j = 0; j < evec.length; j++)
          System.out.println("exc #" + j + " " + evec[j]);
       System.out.println("-----");
    }
  } catch (Throwable e) {
     System.err.println(e);
  }
}

```

[remark]:<slide>(new)
#### Rozbor
V tomto příkladu nejsou získány žádné informace o návratovém typu, protože konstruktéři skutečně nemají skutečný typ návratu.

[remark]:<slide>(wait)
Při spuštění tohoto programu je výstup:

```
name = constructor1
decl class = class constructor1
-----
name = constructor1
decl class = class constructor1
param #0 int
param #1 double
-----
```

[remark]:<slide>(new)
## Práce s atributy
Je také možné zjistit, která atributy jsou definovány ve třídě. 

K tomu lze použít následující kód:

```java
import java.lang.reflect.*;
    
public class field1 {
  private double d;
  public static final int i = 37;
  String s = "testing";
}
```

[remark]:<slide>(new)
```java  
  public static void main(String args[]) {
     try {
        Class cls = Class.forName("field1");
    
        Field fieldlist[] = cls.getDeclaredFields();
        for (int i = 0; i < fieldlist.length; i++) {
           Field fld = fieldlist[i];
           System.out.println("name = " + fld.getName());
           System.out.println("decl class = " + fld.getDeclaringClass());
           System.out.println("type = " + fld.getType());
           int mod = fld.getModifiers();
           System.out.println("modifiers = " + Modifier.toString(mod));
           System.out.println("-----");
        }
      } catch (Throwable e) {
         System.err.println(e);
      }
   }
}
```

[remark]:<slide>(wait)
Jednou novou funkcí je použití modifikátoru. 
- reprezentuje modifikátory, které se nacházejí u atributu (`private int`) 

Modifikátory samotné jsou reprezentovány celočíselným číslem, `Modifier.toString()` se používá k vrácení řetězcové.

[remark]:<slide>(new)
#### Výstupem programu je:

```
name = d
decl class = class field1
type = double
modifiers = private
-----
name = i
decl class = class field1
type = int
modifiers = public static final
-----
name = s
decl class = class field1
type = class java.lang.String
modifiers =
----- 
```

[remark]:<slide>(wait)
Stejně jako u metod je možné získat informace:
- o polích deklarovaných ve třídě (`getDeclaredFields`)
- o polích definovaných v superclasses (`getFields`).

[remark]:<slide>(new)
## Volání metod podle názvu
Reflexi je možné také volat metody podle zadaného jména.

```java
import java.lang.reflect.*;
    
public class method2 {
  public int add(int a, int b) {
     return a + b;
  }
    
  public static void main(String args[]) {
     try {
       Class cls = Class.forName("method2");
       Class partypes[] = {Integer.TYPE, Integer.TYPE};
        Method meth = cls.getMethod("add", partypes);
        method2 methobj = new method2();
        Object arglist[] = {new Integer(37), new Integer(47)};
        Object retobj = meth.invoke(methobj, arglist);
        Integer retval = (Integer)retobj;
        System.out.println(retval.intValue());
     } catch (Throwable e) {
        System.err.println(e);
     }
  }
}
```

[remark]:<slide>(new)
#### Rozbor

Uživatel chce vyvolat metodu `add`, ale to neví až do doby provedení. 

Výše uvedený program ukazuje způsob, jak to udělat.

`getMethod` se používá k nalezení metody ve třídě, která má dva celé typy parametrů a má příslušné jméno. 

Metoda nalezena a uložena do objektu `Method`

Chcete-li vyvolat metodu, musí být sestaven seznam parametrů se základními celočíselnými hodnotami 37 a 47 zabalenými do objektů Integer. 

Návratová hodnota (84) je také zabalena do objektu Integer.

[remark]:<slide>(new)
## Vytváření nových objektů
Konstruktoty požíváme pro vytváření nového objektu.
 
```java
import java.lang.reflect.*;
    
public class constructor2 {
  public constructor2() { }
    
  public constructor2(int a, int b) {
     System.out.println("a = " + a + " b = " + b);
  }
    
  public static void main(String args[]) {
     try {
       Class cls = Class.forName("constructor2");
       Class partypes[] = {Integer.TYPE, Integer.TYPE};
        Constructor ct = cls.getConstructor(partypes);
        Object arglist[] ={new Integer(37), new Integer(47)};
        Object retobj = ct.newInstance(arglist);
     }
     catch (Throwable e) {
        System.err.println(e);
     }
  }
} 
```

[remark]:<slide>(new)
#### Rozbor
Použitím metody `getConstructor` nalezne konstruktor, který zpracovává zadané typy parametrů.
 
Vyvolá je, aby vytvořil novou instanci objektu. 

Hodnota tohoto přístupu je, že je čistě dynamická.

[remark]:<slide>(new)
## Změna hodnot atributů
Dalším použitím reflexe je změna hodnot atributů v objektech. 

Hodnota tohoto parametru je opět odvozena od dynamické povahy reflexe.

Atribut může být vyhledáno jménem v prováděcím programu a atribut jeho hodnotou. 

```java
import java.lang.reflect.*;
    
public class field2 {
  public double d;
    
  public static void main(String args[]) {
     try {
        Class cls = Class.forName("field2");
        Field fld = cls.getField("d");
        field2 f2obj = new field2();
        System.out.println("d = " + f2obj.d);
        fld.setDouble(f2obj, 12.34);
        System.out.println("d = " + f2obj.d);
     } catch (Throwable e) {
        System.err.println(e);
     }
  }
} 
```

V tomto příkladu má atribut `d` hodnotu nastavenou na hodnotu `12.34`.

[remark]:<slide>(new)
## Použití poli
Jedním z konečných použití reflexe je vytvoření a manipulace s poli. 

Pole jsou v jazyce Java specializovaným typem třídy a referenci pole může být přiřazena referenci objektu.

```java
import java.lang.reflect.*;
    
public class array1 {
  public static void main(String args[]) {
     try {
        Class cls = Class.forName("java.lang.String");
        Object arr = Array.newInstance(cls, 10);
        Array.set(arr, 5, "this is a test");
        String s = (String)Array.get(arr, 5);
        System.out.println(s);
     } catch (Throwable e) {
        System.err.println(e);
     }
  }
}
```

Tento příklad vytvoří deset dlouhé pole řetězců a pak nastaví umístění 5 v poli na hodnotu řetězce. 

Hodnota je načtena a zobrazena.

[remark]:<slide>(new)
Složitější manipulace s polem

```java
import java.lang.reflect.*;
    
public class array2 {
  public static void main(String args[]) {
     int dims[] = new int[]{5, 10, 15};
     Object arr = Array.newInstance(Integer.TYPE, dims);
    
     Object arrobj = Array.get(arr, 3);
     Class cls = arrobj.getClass().getComponentType();
     System.out.println(cls);
     arrobj = Array.get(arrobj, 5);
     Array.setInt(arrobj, 10, 37);
    
     int arrcast[][][] = (int[][][])arr;
     System.out.println(arrcast[3][5][10]);
  }
}
```

[remark]:<slide>(new)
#### Rozbor
Tento příklad vytvoří pole 5 x 10 x 15 int.

Potom pokračuje nastavením umístění [3] [5] [10] v poli na hodnotu 37. 

Všimněte si, že multidimenzionální pole je vlastně pole matic, takže například po prvním `Array.get` je výsledkem arrobj pole 10 x 15. 

Toto je znovu odloupeno, aby bylo dosaženo 15-ti dlouhého pole a 10. slot v tomto poli je nastaven pomocí `Array.setInt`.

Všimněte si, že typ vytvořeného pole je dynamický a nemusí být znám při kompilačním čase.

[remark]:<slide>(new)
##  Souhrn
Java reflexe je užitečný, protože podporuje dynamické získávání informací o třídách a datových struktur podle jména a umožňuje jejich manipulaci v rámci prováděcího Java programu. 

Tato funkce je mimořádně silná a nemá žádný ekvivalent v jiných běžných jazycích, jako je C, C ++, Fortran nebo Pascal.

