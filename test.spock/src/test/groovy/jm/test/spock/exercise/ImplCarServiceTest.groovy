package jm.test.spock.exercise

import spock.lang.Specification
import spock.lang.Unroll


class ImplCarServiceTest extends Specification {

    private ImplCarService carService;
    private CarRepository carRepository

    public void setup() {
        carService = new ImplCarService();
        carService.carRepository = carRepository = Mock(CarRepository)
    }

    def "Find all cars test :)"() {
        when:
        def cars = carService.findAll()

        then:
        1 * carRepository.findAllCars()
        cars != null

    }

    def "Create car"() {
        given: "Comment given"
        def car = new Car(name: "Test", id: 123);

        when: "Comment when"
        def result = carService.createCar("Input");

        then: "Comment then"
        1 * carRepository.save(_ as Car) >> {Car c ->
            c.id = 124;
            Optional.of(c)};

        and: "Comment and"
        result.name == "Input";
        result.id == 124


    }

    @Unroll
    def "Create Id #result from name #name"() {
        when:
        int id = carService.createId(name);

        then:
        id == result;

        where:
        name    | result
        "Honza" | 5
        "Pepa"  | 8
    }

    def "Test private plus"() {
        when:
        def r = carService.plus(5, 8)

        then:
        r == 13;
    }
}
