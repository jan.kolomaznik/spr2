package com.example.demo;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DepartmentConfig {

	@Bean
	public TranslateService translateService() {
		return new TranslateService();
	}
	
	@Bean(autowireCandidate = false)
	public DepartmentBean finance() {
		return new DepartmentBean("Finance");			
	}
	
	@Bean()
	public DepartmentBean it(TranslateService service) {
		return new DepartmentBean(service.translate("It"));			
	}
	
	@Bean
	public CommandLineRunner runner() {
		return args -> {
			System.out.println(translateService().translate("msg 1"));
			System.out.println(translateService().translate("msg 2"));
			System.out.format("CommandLineRunner: %s", Arrays.toString(args));
		};
	}
}
