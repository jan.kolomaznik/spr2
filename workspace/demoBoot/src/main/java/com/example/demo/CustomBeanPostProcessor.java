package com.example.demo;

import java.util.Arrays;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//@Service
public class CustomBeanPostProcessor  implements BeanPostProcessor {
	
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println(Arrays.toString(bean.getClass().getDeclaredAnnotations()));
    	System.out.println("PostProcessBeforeInitialization() for :" + beanName);
        return bean;
    }
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("PostProcessAfterInitialization() for :" + beanName);
        return bean;
    }

}
