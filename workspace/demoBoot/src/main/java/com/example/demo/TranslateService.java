package com.example.demo;

import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@MyComponent
//@Scope("prototype")
public class TranslateService implements InitializingBean, DisposableBean {
	
	
	private static AtomicInteger nextId = new AtomicInteger();
	
	private int id = nextId.incrementAndGet();
	
	public String translate(String message) {
		return String.format("Service %d say: %s", id, message);
	}
	
	@PostConstruct
	private void init() {
		System.out.format("init: service %d.%n", id);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.format("afterPropertiesSet: service %d.%n", id);		
	}

	@Override
	public void destroy() throws Exception {
		System.out.format("destroy: service %d.5n", id);		
	}
}
