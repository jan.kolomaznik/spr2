package com.example.demo;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
@ShellComponent
public class DemoBootApplication {
	
	@Autowired
	private ApplicationContext ac;

	public static void main(String[] args) {
		SpringApplication.run(DemoBootApplication.class, args);
	}
	
	
	@ShellMethod("Hello world greeting.")
	public String hello(String msg) {
		TranslateService ts = ac.getBean(TranslateService.class);
		return ts.translate(msg);
	}
	
	@ShellMethod("List of beans")
	public String beans() {
		return Stream.of(ac.getBeanDefinitionNames())
				.collect(Collectors.joining("\n"))
				+ "\n\n" + ac.getParent();
	}
	
	@ShellMethod("List of my beans")
	public String myBeans() {
		ApplicationContext myAC = new ClassPathXmlApplicationContext("beans.xml");
		return Stream.of(myAC.getBeanDefinitionNames())
				.collect(Collectors.joining("\n"));
	}
	
	@ShellMethod("Autowired demo")
	public String autowired(String name) {
		
		return ac.getBean(name).toString();
	}
	

}
