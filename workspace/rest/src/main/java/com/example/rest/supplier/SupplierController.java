package com.example.rest.supplier;

import com.example.rest.product.Product;
import com.example.rest.product.ProductRepository;
import com.example.rest.utils.NotFoundException;
import com.example.rest.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
public class SupplierController {

    @Autowired
    private SupplierRepository repository;

    @GetMapping("/supplier")
    public Iterable<Supplier> getAll() {
        return repository.findAll();
    }

    @GetMapping("/supplier/{id}")
    public Response<Supplier> getOne(@PathVariable UUID id) {
        Optional<Supplier> optional = repository.findById(id);
        Supplier supplier = optional.orElseThrow(NotFoundException::new);
        return new Response<>(supplier)
                .addLink(linkTo(methodOn(SupplierController.class).getOne(id)).withSelfRel());
    }

}
