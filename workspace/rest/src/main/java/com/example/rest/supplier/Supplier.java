package com.example.rest.supplier;

import com.example.rest.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Supplier {

    @Id
    @Type(type = "uuid-char")
    private UUID id;

    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "supplier")
    private List<Product> products;
}
