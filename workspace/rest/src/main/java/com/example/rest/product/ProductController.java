package com.example.rest.product;

import com.example.rest.supplier.SupplierController;
import com.example.rest.supplier.SupplierRepository;
import com.example.rest.utils.NotFoundException;
import com.example.rest.utils.PageResponse;
import com.example.rest.utils.Response;
import io.swagger.annotations.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api")
@Api(tags = "/api/product/")
public class ProductController {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private SupplierRepository supplierRepository;

    @GetMapping("/product")
    @Transactional()
    public PageResponse<Product> getPage(
            @RequestParam int page,
            @RequestParam int size
    ) {
        return new PageResponse<Product>(
                repository.findAll(PageRequest.of(page, size))
        );
    }

    @GetMapping("/product/{id}")
    public Response<Product> getOne(@PathVariable UUID id) {
        Optional<Product> optional = repository.findById(id);
        Product product = optional.orElseThrow(NotFoundException::new);
        return new Response<>(product)
                .addLink(linkTo(methodOn(ProductController.class).getOne(id)).withSelfRel())
                .addLink(linkTo(methodOn(SupplierController.class).getOne(product.getSupplier().getId())).withRel("supplier"));
    }

    @Data
    public static class PostProductRequest {

        @ApiModelProperty(
                notes = "Product name",
                example = "IOT Chip"
        )
        @Size(min = 5, max = 255)
        private String name;

        @ApiModelProperty(
                notes = "Existing Supplier uuid",
                example = "77ef20e6-c811-47e4-8d8f-1ffe02fa38a1"
        )
        @NotNull
        private UUID supplier;
    }

    @ApiOperation("Create new product")
    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Response<Product> post(
            @Validated @RequestBody PostProductRequest productRequest) {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName(productRequest.name);
        product.setCreated(LocalDate.now());
        product.setSupplier(supplierRepository
                    .findById(productRequest.supplier)
                    .orElseThrow(NotFoundException::new));

        product = repository.save(product);

        return new Response<>(product)
                .addLink(linkTo(methodOn(ProductController.class).getOne(product.getId())).withSelfRel())
                .addLink(linkTo(methodOn(SupplierController.class).getOne(product.getSupplier().getId())).withRel("supplier"));

    }

}
