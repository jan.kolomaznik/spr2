package com.example.rest.utils;

import lombok.Getter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
public class PageResponse<T> {

    private List<T> items;

    private int totalPage;

    private long totalItems;

    private int currentPage;

    public PageResponse(Page page) {
        this.items = page.getContent();
        this.totalPage = page.getTotalPages();
        this.totalItems = page.getTotalElements();
        this.currentPage = page.getNumber();
    }
}
