package com.example.rest.utils;

import lombok.Getter;
import org.springframework.hateoas.Link;

import java.util.LinkedHashMap;
import java.util.Map;

@Getter
public class Response<T> {

    private T payload;

    private Map<String, String> links = new LinkedHashMap<>();

    public Response(T payload) {
        this.payload = payload;
    }

    public Response<T> addLink(Link link) {
        links.put(link.getRel(), link.getHref());
        return this;
    }
}
