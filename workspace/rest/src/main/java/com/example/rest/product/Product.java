package com.example.rest.product;

import com.example.rest.supplier.Supplier;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
public class Product {

    @Id
    @Type(type = "uuid-char")
    private UUID id;

    private String name;

    private LocalDate created;

    @ManyToOne
    @JsonIgnore
    private Supplier supplier;

    public UUID getSupplierID() {
        return supplier.getId();
    }
}
