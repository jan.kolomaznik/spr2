package com.example.rest.supplier;

import com.example.rest.product.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SupplierRepository extends PagingAndSortingRepository<Supplier, UUID> {
}
