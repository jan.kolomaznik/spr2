INSERT INTO SUPPLIER (ID, NAME) VALUES
  ('d2c3ef5d-d54e-423d-8973-5039642e17c4', 'Name 1'),
  ('94dd1be9-1157-41f7-baae-14681c8e60f8', 'Name 2'),
  ('28afdc63-3bfa-42a3-a3b3-abb6cf5f4711', 'Name 3'),
  ('77ef20e6-c811-47e4-8d8f-1ffe02fa38a1', 'Name 4');

INSERT INTO PRODUCT (ID, NAME, SUPPLIER_ID ) VALUES
  ('2940ecb0-eeee-4d7b-b26c-4ae04a8c5f33', 'Product 1', 'd2c3ef5d-d54e-423d-8973-5039642e17c4'),
  ('33d13832-6d97-4e66-883e-db3fa25213c0', 'Product 2', 'd2c3ef5d-d54e-423d-8973-5039642e17c4'),
  ('6f585b38-63f6-41b5-bca5-7b15e84afd6d', 'Product 3', 'd2c3ef5d-d54e-423d-8973-5039642e17c4'),
  ('e3590adf-4486-41ee-b14c-a06082c39735', 'Product 4', '94dd1be9-1157-41f7-baae-14681c8e60f8'),
  ('a4416c0e-cc62-478d-b704-db8d6273b07e', 'Product 5', '94dd1be9-1157-41f7-baae-14681c8e60f8');