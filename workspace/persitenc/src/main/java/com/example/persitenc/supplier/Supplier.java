package com.example.persitenc.supplier;

import com.example.persitenc.product.Product;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Supplier {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @Column(unique = true, nullable = false)
    private String name;

    @OneToMany(mappedBy = "supplier")
    private List<Product> products;
}
