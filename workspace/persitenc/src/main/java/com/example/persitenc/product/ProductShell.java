package com.example.persitenc.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@ShellComponent
public class ProductShell {

    @Autowired
    private ProductService productService;

    @ShellMethod("Find all product")
    public String findAllProduct() {
        return StreamSupport.stream(productService.findAll().spliterator(), false)
                .map(Product::toString)
                .collect(Collectors.joining("\n"));
    }

    @ShellMethod("Create product")
    public String createProduct(String name, String dest) {
        return productService
                .create(name, dest)
                .toString();
    }

    @ShellMethod("Add tag to product")
    public String addTag(UUID product, String tag) {
        return productService
                .addTag(product, tag)
                .toString();
    }
}
