package com.example.persitenc.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Iterable<Product> findAll() {
        return productRepository.findAll();
    }

    public Product create(String name, String description) {
        Product product = new Product();
        product.setName(name);
        product.setDescription(description);
        product.setCreated(new Date());
        return  productRepository.save(product);
    }

    public Product addTag(UUID uuid, String tag) {
        Product product = productRepository
                .findById(uuid)
                .orElseThrow(IllegalArgumentException::new);
        product.getTags().add(tag);
        return productRepository.save(product);
    }
}
