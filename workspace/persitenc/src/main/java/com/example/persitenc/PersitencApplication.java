package com.example.persitenc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersitencApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersitencApplication.class, args);
    }



}
