package com.example.persitenc.supplier;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface SupplierRepository extends PagingAndSortingRepository<Supplier, UUID> {

}
