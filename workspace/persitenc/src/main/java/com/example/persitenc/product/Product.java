package com.example.persitenc.product;

import com.example.persitenc.supplier.Supplier;
import lombok.Data;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Product {

    public enum Category {
        FOOD, IOT, CARS
    }

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> tags;

    private String name;
    private String description;
    private Boolean availability;
    private Date created;

    @Enumerated(EnumType.STRING)
    private Category category = Category.IOT;

    public List<String> getTags() {
        return tags != null
                ? tags
                : (tags = new ArrayList<>());
    }

    @ManyToOne
    private Supplier supplier;
}
