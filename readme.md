SPRING FRAMEWORK - PRO POKROČILÉ
================================

V rámci tohoto školení se seznámíte s pokročilými možnostmi vývoje J2EE aplikací v prostředí Spring Framework. Kurz Vás seznámí s možnostmi využití Maven, Java Persistence API (JPA), Java Persistence Query Language (JPQL) a prací ve Spring MVC. 

Osnova kurzu 
------------

- **Zhrnutí základního kurzu**
   - Co je to Spring, základní přehled komponent
   - Spring Boot a Spring Initializr
   - Magické zkratky IoC a DI
   - Základní pojmy
   
- **Spring context**

- Maven/Gradle
- **Java Persistence API **
  - koncept entitních tříd
  - perzistentní jednotky
  - API entitního správce
  - aplikace Hibernate jako JPA poskytovatele
  - Java Persistence Query Language 
  
- **Transakce ve Springu a řízení z aplikace**

- **~~Validace na úrovni entitních třid podle JSR 303~~**

- **~~Koncept lokalizace ve Spring MVC~~**

- **Spring MVC**

- **Pretty URL mapping v MVC**

- **Ladění aplikací**
    
               [remark]:<slide>(new)
# Zadání
	Tvorime si uvnitr aplikace programaticky dynamicky vlastni spring context – potrebuju aby se s lidma probraly takove ty “interni strivka springu”.
	hierarchie applikacnich kontextu v aplikaci
	Spring expression language – k cemu kdy a proc
•    Spring-context, Spring-beans
	bean definition vs.
bean instance – jake jsou moznosti definovani spring contextu a jak je pouzivat v ramci jednoho kontextu
	jak vypada bean definition a jak ji treba za behu pridat
	Jak resit spring missing bean – likely problem in context scanning, nebo missing dependency….
	spring annotation processing – proc a jak spring pouziva proxy a co to je…
	@Service vs.
  Jak udelat nejakou vlastni AutoConfigure @EnableSomething anotaci – viz.
@Import
Annotace + annotation procesing + vlastní spring annotace + Java reflectio + AOP
