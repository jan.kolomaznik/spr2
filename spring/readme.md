[remark]:<class>(center, middle)
# Spring Framework

[remark]:<slide>(new)
## Úvod
Spring je light a otevřený framework vytvořený Rodem Johnsonem v roce 2003.

Spring je jeden z nejrozšířenějších frameworků ve světe enterprise java. 

Jeho možnosti se rozrostly do obřích rozměrů a obsáhnout všechny jeho části už snad není ani v lidských silách. 

Spring boot je projekt, který využití Spring framework a navazujících knihoven usnadňuje a urychluje. 

Přináší princip convention over configuration a slouží k extrémně rychlému vyrábění aplikací postavených na springu. 

Spring cloud pak do tohoto světa připojuje mnoho moderních a populárních technologií ze světa microservices. 

[remark]:<slide>(new)
## Spring vs JavaEE(EJB)

Je alternativou k JavaEE, ale je jednodušší na použití. 

Není nutné využít všechno co nabízí najednou, lze si vybírat pouze požadovanou funkcionalitu. 

Také na rozdíl od JavaEE nevynucuje závislost na konkrétních třídách.

Je silně modulárni

Vychází z **IoC** a **Dependici injection** frameworku

Aktální verze: **5.1.6 GA**

Dokumence dostulná na stránkách: [link](https://docs.spring.io/spring/docs/current/spring-framework-reference/index.html)

[remark]:<slide>(new)
## Spring framework je ...

Spring je komplexní a modulární framewok a může být snadno použit pro implementaci všech vrsev aplikace.

Spring je velmi volný a invazivní, nenutí programátora implementovat vlastní třídy rošířením rozhraní nebo tríd ze Springku.

Spring vytužívá POJO třídy.

Díky Spring Framework je vývoj aplikací J2EE mnohem jednodušší

[remark]:<slide>(wait)
#### Spring má tyto základní výhody: 

- Jednoduchost
- Testability
- Volné spojení

[remark]:<slide>(new)
## Spring boot je ...

Spring Boot není framework.
 
Spring boot je způsob, jak snadno vytvořit samostatnou aplikaci s minimálními nebo nulovými konfiguracemi. 

Poskytuje výchozí nastavení konfigurace kódů a anotací pro rychlé spuštění nových projektů ve Spring frameworku. 

Spring Boot využívá stávajících Spring projektů, ale i projektů třetích stran, které integruje. 

Poskytuje soubor konfigurací **Starter** pro Maven nebo Gradl, které lze použít k přidání požadovaných závislostí a také k automatické konfiguraci.

Funkce Spring Boot automaticky konfiguruje požadované třídy v závislosti na knihovnách na své třídě cesty. 

[remark]:<slide>(wait)
#### Příklad: *Předpokládejme, že vaše aplikace chce komunikovat s databází DB.*
- jestliže existují Spring Data library v projektu
- pak automaticky nastaví připojení k DB společně s třídou pro přístup k nim.

[remark]:<slide>(new)
### Spring Boot má tyto základní výhody:     
- Je velmi snadné vyvinout aplikace napsané ve Spring frameworku
- Podporuje programovací jazyky: Java, Groovy, Kotlin.
- Spring Boot urchluje vývoj a zvyšuje produktivitu.
- Snižuje potřebné množství anotací a konfigurace XML.
- Velmi snadné je integrace Spring Boot Application s Spring JDBC, Spring ORM, Spring Data, Spring Security
- Nabízí zabudované HTTP servery jako Tomcat, Jetty atd.
- Poskytuje nástroj CLI (Command Line Interface), který umožňuje velmi snadno a rychle vyvíjet.
- Spring Boot poskytuje spoustu zásuvných modulů pro vývoj a testování pomocí nástrojů Maven a Gradle
- Poskytuje spoustu zásuvných modulů pro práci s vestavěnými a paměťovými databázemi.

[remark]:<slide>(new)
### Spring Boot se stará o ...   

![](../spring/media/Configuration.png)

[remark]:<slide>(new)
## [Spring Initializr](https://start.spring.io/)

Jedná se o webový nástroj, který poskytuje Spring na oficiálních stránkách. 

![](media/Initializr.png)

[remark]:<slide>(new)
### Příklad: *Vytvořte projekt pomocí  Spring Initializr*
Pomocí Spring Initializr vytvoříme jednoduchý projekt, který bude obsahovat následující moduly:

- DevTools
- Lombok
- Spring Shell

[remark]:<slide>(wait)
1. K hlavní třídě přidejte annotaci 
```java
@ShellComponent
```

2. Vytvořte metodu:
```java
@ShellMethod("Say hello")
public String sayHello() {
    return "Hello world";
}
```

3. Spusťě aplikaci za zadejte příkazy:
```java
shell:>help
shell:>say-hello
```

[remark]:<slide>(new)
### Spring Runtime moduly
Spring Framework poskytyhe mnoho modulu.

- **Test**: Poskytuje podporu testování s JUnit a TestNG.
- **Spring Core Container**: 
  - Obsahuje moduly core, bean, context a expression language (EL) (EL).
  - **Core a Beans**: poskytují funkce IOC a Dependency Injection.
  - **Context**: podporuje internacionalizaci (I18N), EJB, JMS, Basic Remoting.
  - **Expression Language**: Jedná se o rozšíření EL definované v JSP. 
     Poskytuje podporu při nastavování hodnot vlastností, vyvolávání metod, přístupu k kolekcím a indexerům, pojmenovaných proměnných, logických a aritmetických operátorů, načítání objektů podle názvu atd.
- **AOP, Aspekty a instrumentace**: 
  - implementaci a tvorva aspektů: [wiki](https://en.wikipedia.org/wiki/Aspect-oriented_programming)
  - Dvě implementace "Spring" a [AspectJ](https://www.eclipse.org/aspectj/).
- **Data Access / Integration**: přístup k datům a databázím: JDBC, ORM, OXM, JMS a Transakce. 
- **Web** Podporoa tvorby webových aplikací

[remark]:<slide>(new)
![](media/Spring-overview.png)

[remark]:<slide>(new)
### Rozcestní: Odkazy na dokumentaci 

- **[Core](https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#spring-core)**	
  IoC container, Events, Resources, i18n, Validation, Data Binding, Type Conversion, SpEL, AOP.

- **[Testing](https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html#testing)**	
  Mock objects, TestContext framework, Spring MVC Test, WebTestClient.

- **[Data Access](https://docs.spring.io/spring/docs/current/spring-framework-reference/data-access.html#spring-data-tier)**	
  Transactions, DAO support, JDBC, ORM, Marshalling XML.

- **[Web Servlet](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#spring-web)**	
  Spring MVC, WebSocket, SockJS, STOMP messaging.

- **[Web Reactive](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html#spring-webflux)**	
  Spring WebFlux, WebClient, WebSocket.

- **[Integration](https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#spring-integration)**	
  Remoting, JMS, JCA, JMX, Email, Tasks, Scheduling, Cache.
  
[remark]:<slide>(new)
### Vazby a třívrstvá architekura
1. Spring podporuje slabé vazby v kódu.

2. Spring podporuje třívrstvou architekturu.

[remark]:<slide>(wait)
![](media/3-tier-architechture.png)

[remark]:<slide>(new)
[remark]:<class>(center, middle)
# Dependency injection
# Inversion of Control

[remark]:<slide>(new)
### Pevným vazbám se snažíme vyhýbat!
```java
public interface CarDao {}

public class CarDaoImpl implements CarDao {}

// zde mezi CarDao a CarService je pevná vazba
public class CarServiceImpl {
    private CarDao carDao = new CarDaoImpl();
}
```

[remark]:<slide>(wait)
**Proč?** 
- Vaše architektura je příliš úzce svázaná (podobně jako u dědičnosti) a málo flexibilní. 
- Změna kódu může být velice obtížná. 
- Třída bez pevných vazeb se lépe testuje.

[remark]:<slide>(wait)
**Jak?** 
- Princip IoC přesouvá odpovědnost za vznik vazeb na někoho jiného. 
- V našem případě je přesunut z programátora na framework.


[remark]:<slide>(new)
## Základní seznámení?
Technika pro vkládání závislostí mezi jednotlivými komponentami programu.

Vytváří vazby mezi komponenty místo programátora. 

Dependency injection lze chápat jako novější název pro Inversion of Control. 

[remark]:<slide>(wait)
#### Mezi výhody patří:
- *Lepší přehlednost kódu* – na první pohled jsou zřejmé závislosti.
- *Jednodušší struktura aplikace* – uplatňování principů IoC vede k vytváření opakovaně použitelných komponent.
- *Opakovaná použitelnost* – třídy (komponenty) využívající DI pro nastavení závislostí jsou snadněji opakovaně použitelné v jiných prostředích či aplikacích.
- *Lepší testovatelnost* – třídy se dají lépe testovat a snadno mockovat.

[remark]:<slide>(wait)
#### Nevýhody (rizika) jsou:
- Pro vývojáře, který není s principy IoC obeznámen, může být zpočátku složitější se v takto napsaném kódu orientovat.
- Využití DI kontejneru může mít vliv na rychlost aplikace, zvlášť v prostředích nepodporujících vícevláknové zpracování.


[remark]:<slide>(new)
## Dependency injection (DI)
Komponenty jsou odstíněny od této správy vazeb (závislostí)

Tuto odpovědnost přebírá dependency provider neboli kontejner. 

DI zahrnuje tedy nejméně tři objekty, které spolu musí spolupracovat. 

- **Konzument**, tedy objekt požadující služby. 
- **Poskytovatele** těchto služeb mu dodá DI provider, který ve skutečnosti zodpovídá za celý životní cyklus poskytovatele. 
- **Provider** neboli injector může být implementován několika způsoby, např. jako lokátor služeb, abstraktní továrna, tovární metoda, nebo pomocí nějakého z řady z frameworků, například Spring.

[remark]:<slide>(wait)
[Martin Fowler](https://martinfowler.com/) poukazuje na tři různé vzory, jak lze objektu přidat externí referenci jiného objektu.

1. Vkládání rozhraním – externí modul, který je do objektu přidán, implementuje rozhraní, jež objekt očekává v době sestavení programu.
1. Vkládání setter metodou – objekt má setter metodu, pomocí níž lze závislost injektovat.
1. Injekce konstruktorem – závislost je do objektu injektována v parametru konstruktoru již při jeho zrodu.
    
[remark]:<slide>(new)
## Dependency injection s Spring Framework
Spring v základu vznikl jeko DI Framework.

Spring podporuje všechny tři typy **injekce** a nekolik způsobů konfigurace.
Pro přehlednost a větší flexibilitu je dobré mít oddělenu konfiguraci od implementace. 

1. **Existují tři typy injekce a označuje je jako**:
  - Property inject
  - Constructor inject
  - Setter inject
  
2. **Spring podporuje několik způsobů konfigurace**:
  - XML (legacy)
  - Anotacemi
  - Definicí v kodu (metodou)

[remark]:<slide>(new)
### Příklad: *Jednoduchá injekce*
1. Vytvoříme službu `TranslateService` s metodou `public String translate(String msg)`.

2. Po konfiguraci Springu použijeme metodu pomocí annotaci: `@Component`.

3. Do hlavní třídy pomocí annotace `@Autowired` injektujeme dependenci na `TranslateService`

4. Vytvoříme `@ShellMethod`, ktará tuto závislost použije:
```java
@ShellMethod("Translate message")
public String translate(String msg) {
    return translateService.translate(msg);
}
```

[remark]:<slide>(new)
## Spring: základní pojmy
Zde je nutné znát dva pojmy:

[remark]:<slide>(wait)
### Bean
Objekt, který vykonává nějakou funkčnost (např. přidává data do databáze, vyhledává...). 

Beany žijí v kontejneru (context). 

Lze s ním pracovat v celé aplikaci. 

Existují dva typy bean (scope).
- **Singleton** objekt je v celé aplikaci jen jednou. 
  Pokaždé, pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme stejnou instanci.
- **Prototype** je podobný jako singleton. 
  Rozdíl je v tom, že pokud si řekneme o daný objekt aplikačnímu kontextu, dostaneme vždy novou instanci.

[remark]:<slide>(wait)
#### Příklad: *Práce se scope*

[remark]:<slide>(new)
### Context
Jedná se kontejner, neboli aplikačním kontextu, ve kterém objekty (BEAN), které tvoří funkční jádro vaší aplikace. 

Context se zavádí při startu aplikace a reprezentuje ho třída `ApplicationContext`. 

V celé aplikaci je jen jeden a dá se injektovat odkudkoli.

**ApplicationContext** poskytuje:
- Bean factory metody pro přístup k aplikačním komponentám.
- Schopnost načíst prostředky souboru obecným způsobem.
- Možnost publikovat události registrovaným posluchačům.
- Schopnost řešit zprávy, podporovat internacionalizaci.
- Dědičnost z rodičovského kontextu. 
  Definice v potomku kontextu budou mít vždy přednost. 

**Implementace**:
`AnnotationConfigApplicationContext`, 
`AnnotationConfigWebApplicationContext`, 
`ClassPathXmlApplicationContext`, 
`FileSystemXmlApplicationContext`, 
`ResourceAdapterApplicationContext`, 
`StaticApplicationContext`, 
`StaticWebApplicationContext`, 
`XmlWebApplicationContext`, ...
  
[remark]:<slide>(new)
### Základní annotace
Je realizována obyčejnou Java třídou, ve které jsou použity anotace pro tvorbu aplikačního kontextu. 

Je dobré, si pro konfigurační třídy udělat speciální package (configuration) nebo je umistit do "základního" balíčku.

- **`@Configuration`** vytvoří z dané třídy konfigurační třídu
- **`@Import`** spojí dvě konfigurace (naimportuje jinou konfiguraci)
- **`@Bean`** vytvoří beanu; typ je návratová hodnota a název je název metody (pokud se nepoužije name). 
  Lze i změnit defaultní singleton scope (`scope=DefaultSco­pes.PROTOTYPE`)
- **`@Autowired`** injektuje instanci jiné beany
- **`@ComponentScan`** - proskenuje zadané package. 
  Pokud narazí na speciální anotace:
  - `@Controller`: prezentační vrstva; 
  - `@Service`: aplikační vrstva; 
  - `@Repository`: datová vrstva) vytvoří z daných tříd beany. 
  - *Užitečná věc pro rychlou tvorbu bean.*

[remark]:<slide>(new)
### Příklad: *Použití kontejneru*
Pro práci s aplikačním kontextem slouží beana `ApplicationContext`. 

Tato bean má metodu `getBean()`, pomocí níž získáte jakoukoli beanu z kontejneru.

```java
@Configuration
public class ContextConfig {
    @Bean
    public NameStrategy nameStrategy() {
            return new NameStrategyImpl();
    }
}

public class UpdateFactoryImpl implements UpdateFactory {
    // zisk pristupu ke kontejneru
    @Autowired
    private ApplicationContext applicationContext;

    public Strategy getStrategy(Change change) {
    if (change.isChangeName()) {
        // vytažení beany NameStrategy
        return applicationContext.getBean(NameStrategy.class);
        }
    }
}
```

*Příklad je výtažek kódu, kde je využit návrhový vzor Factory. 
Podle změny (change) se rozhoduje kterou strategii má factory vytvořit (vytáhnout z aplikačního kontextu). 
V našem případě se jedná o NameStrategy.*