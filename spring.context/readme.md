[remark]:<class>(center, middle)
# Spring context

[remark]:<slide>(new)
# BeanFactory and ApplicationContext
**Spring IoC container** je základem Spring Frameworku.

Kontejner vytvoří objekty, propojuje je, nakonfiguruje a spravuje jejich celý životní cyklus.


Kontejner Spring používá injekční závislost (DI) ke správě komponent, které tvoří aplikaci.
  
Spring poskytuje dva odlišné typy kontejnerů.
1.
`BeanFactory` kontejner
2.
`ApplicationContext`

[remark]:<slide>(new)
## BeanFactory
BeanFactory je rozhraní pro vytváření bean.
 
Je schopné udržovat senzam vyvořených bean.

Umí nastavovat jejich závislostit.

[remark]:<slide>(wait)
Pro definici bean se nejčastejí používá xml soubor: `XmlBeanFactory`
```java
InputStream is = new FileInputStream("beans.xml");
BeanFactory factory = new XmlBeanFactory(is);
 
//Get bean
HelloWorld obj = (HelloWorld) factory.getBean("helloWorld");
```

Pomocí metody `getBean(String)` můžete získat referenci na všchny instance bean.

[remark]:<slide>(new)
### BeanFactory metody
Rozhraní BeanFactory má pouze šest metod:

1.
`boolean containsBean(String)`: vrací true, pokud BeanFactory obsahuje bean.

2.
`Object getBean(String)`: pro získání instance beanu podle jmeána.

3.
`Object getBean(String, Class)`: vrátí bean, registrovaný pod daným jménem a typem.

4.
`Class getType(String name):` vrátí třídu beanu s daným jménem.


5.
`booleanisSingleton(String)`: Určuje, zda definice beanu je nebo není singleton.


6.
`String[] getAliases(String)`: Vrátí aliasy pro daný bean.

[remark]:<slide>(new)
## Spring ApplicationContext container
**ApplicationContext** přidává více funkcí specifických pro enteprise aplikace.

- Pracovat s konfiguračními soubory
- publikovat události
- ...

Tento kontejner je definován rozhraním `org.springframework.context.ApplicationContext`.

Kontejner ApplicationContext obsahuje všechny funkce kontejneru BeanFactory

BeanFactory lze stále používat pro lehké aplikace, jako jsou mobilní zařízení, kde je objem a rychlost dat významná.

[remark]:<slide>(new)
### Implementace ApplicationContext
Mezi nejčastěji používané implementace ApplicationContext patří:

- `FileSystemXmlApplicationContext`:
  Tento kontejner načte definice ze XML.

  Zde musíte poskytnout konstruktoru úplnou cestu konfiguračního souboru XML bean.

- `ClassPathXmlApplicationContext`:
  Tento kontejner načte definice z XML.

  Zde nemusíte poskytovat úplnou cestu k souboru XML, ale musíte správně nastavit CLASSPATH, protože tento kontejner bude vypadat jako konfigurační soubor XML v CLASSPATH.

- `WebXmlApplicationContext`:
  Tento kontejner načte soubor XML s definicemi všech fazolí z webové aplikace.

- `AnnotationConfigApplicationContext`: 
  Tento context přebírá konfigurace ze třídy označené annotaci `@Configuration` a nebo `@Component`

- `StaticApplicationContext`:
  Tento kontext je vhodná pro definici bean z kodu, což je vhodné například pro testoavnání.
  
- `GenericGroovyApplicationContext`:
  Definice bean v jazyce Groovy
   
[remark]:<slide>(new)
### Použití ApplicationContext
Ukázkový kód pro instanci kontextu aplikace bude vypadat takto.

```java
ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");
HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
```

[remark]:<slide>(new)
### BeanFactory or ApplicationContext?
| Feature | BeanFactory | ApplicationContext |
| :--- | --- | --- |
| Bean instantiation/wiring | Yes | Yes |
| Integrated lifecycle management | No | Yes |
| Automatic BeanPostProcessor registration | No | Yes |
| Automatic BeanFactoryPostProcessor registration | No | Yes |
| Convenient MessageSource access (for internalization) | No | Yes |
| Built-in ApplicationEvent publication mechanism | No | Yes |

[remark]:<slide>(new)
# Jak Spring implementuje IoC
V objektově orientovaném programování existuje několik základních technik pro implementaci IoC.
 
- pomocí Factory pattern
- pomocí service locator pattern
- pomocí dependency injection, které se dále dělí na:
  - constructor injection
  - setter injection
  - interface injection

Balíčky `org.springframework.beans` a `org.springframework.context` poskytují základ pro IoC kontejner Spring Framework.


Rozhraní `BeanFactory` je implementaci IoC ve Spring 

Rozhraní `ApplicationContext` navazuje na `BeanFactory` a přidává další funkce.
- jako je snadnější integrace s funkcemi Spring AOP, 
- zpracování prostředků zpráv (pro použití v internacionalizaci), 
- propagace událostí a 
- specifických kontextů aplikací.

[remark]:<slide>(new)
## BeanFactory
`BeanFactory` je skutečná implementace Spring IoC kontejneru.

![](media/container-magic.png)

[remark]:<slide>(new)
## Jak se vytvářejí Beans
Definici beanu lze považovat za návod k vytvoření jednoho nebo více skutečných objektů.
 
Kontejner při dotazu podle definice beans vytváři nové instance nebo vrací již existující.

K vytvoření instance lze použit:
- Konstruktor
- Statickou factory metodu
- Tovární třídu

[remark]:<slide>(new)
### Konstruktor
Při vytváření beanu pomocí přístupu konstruktoru lze použít libovolnou třídu s bezparamterickým konstruktorem.

To znamená, že vytvořená třída nemusí implementovat žádná specifická rozhraní nebo být kódována specifickým způsobem.


Stačí uvést nazev benu a jeho třídu.


Při použití metadat konfigurace založených na XML můžete zadat svou třídu bean takto:

```xml
<bean id="exampleBean" 
      class="examples.ExampleBean"/>

<bean name="anotherExample" 
      class="examples.ExampleBeanTwo"
      alias="another"/>
```
[remark]:<slide>(new)
### Statickou factory metodu
Při definování beanu, muže být určena statické tovární metoda třídy beanu.

```xml
<bean id="clientService"
      class="examples.ClientService"
      factory-method="createInstance"/>
```

Třída:
```java
public class ClientService {
    private static ClientService clientService = new ClientService();
    private ClientService() {}

    public static ClientService createInstance() {
        return clientService;
    }
}
```

Spring očekává, že bude moci tuto metodu zavolat a že vrací instanci beanu.

Takto vytvořený objekt je dále zprcováván tak, jako by byl vytvořen přeš konstruktor.

[remark]:<slide>(new)
### Tovární třídu
Dalším způsobem je vytvoření pomocí instance pomocí metody továrny.

Pro vytvoření beanu je vyvolána tovární metoda existujícího beanu z kontejneru.

```xml
<!-- the factory bean, which contains a method called createInstance() -->
<bean id="serviceLocator" 
      class="examples.DefaultServiceLocator"/>

<!-- the bean to be created via the factory bean -->
<bean id="clientService"
      factory-bean="serviceLocator"
      factory-method="createClientServiceInstance"/>
```

```java
public class DefaultServiceLocator {

    private static ClientService clientService = new ClientServiceImpl();

    public ClientService createClientServiceInstance() {
        return clientService;
    }
}
```

[remark]:<slide>(new)
## Dependency Injection
Základním principem Dependency Injection (DI) je to, že objekty definují své závislosti pouze prostřednictvím:
1.
argumentů konstruktoru, 
2.
argumentů na tovární metodu,
3.
nebo pomoci properties

Spring používá k definování:
1.
metadata v xml
2.
annotaci `@Autowired`

Je to pak úloha kontejneru, aby tyto závislosti skutečně injektovala, když vytvoří bean.


Toto je v zásadě inverzní, tedy název Inverze řízení (IoC).

[remark]:<slide>(wait)
#### Typy Injekce
  - Attribute inject
  - Constructor inject
  - Setter inject
  
[remark]:<slide>(new)
### Attribute inject 
Framework si sám najde danou property pomocí reflexe.

```java
@Autowired
private CarDao carDao;
```

*Toto nemá defakto obdobu v xml*

[remark]:<slide>(wait)
#### Příklad: ...

[remark]:<slide>(new)
### Constructor inject
Při vytváření pošle přes konstruktor instance potřebných component.

```java
private CarDao carDao;

@Autowired
public CarServiceImpl(CarDao carDao) {
        this.carDao = carDao;
}
```

[remark]:<slide>(wait)
#### Příklad: pomocí xml 1: java
```java
package x.y;

public class ThingOne {

    public ThingOne(ThingTwo thingTwo, ThingThree thingThree) {
        // ...
    }
}
```

[remark]:<slide>(new)
#### Příklad: pomocí xml 1: xml
```xml
<beans>
    <bean id="beanOne" class="x.y.ThingOne">
        <constructor-arg ref="beanTwo"/>
        <constructor-arg ref="beanThree"/>
    </bean>

    <bean id="beanTwo" class="x.y.ThingTwo"/>

    <bean id="beanThree" class="x.y.ThingThree"/>
</beans>
```
[remark]:<slide>(wait)
#### Příklad: pomocí xml 2: java
```java
package examples;

public class ExampleBean {

    private int years;
    private String ultimateAnswer;

    public ExampleBean(int years, String ultimateAnswer) {
        this.years = years;
        this.ultimateAnswer = ultimateAnswer;
    }
}
```

[remark]:<slide>(new)
#### Příklad: pomocí xml 2: java
```xml
<bean id="exampleBean" class="examples.ExampleBean">
    <constructor-arg type="int" value="7500000"/>
    <constructor-arg type="java.lang.String" value="42"/>
</bean>
```

[remark]:<slide>(wait)
### Setter inject
Při vytváření je nahrána instance pomocí setter-u.

```java
private CarDao carDao;

@Autowired
public void setCarDao(CarDao carDao) {
        this.carDao = carDao;
}
```

[remark]:<slide>(wait)
#### Příklad: ...
 
[remark]:<slide>(new)
# Spring bean scopes 
Spring definuje 5 bean scope:
- `singleton`, 
- `prototyp`, 
- `request`, 
- `session`,
- `global-session`.
 
Definují rozsah životního cyklu beanu.

První dva jsou použitlené vždy, ostatní tří pouze v kombinaci 
- `WebBeanFactory` nebo 
- `WebApplicationContext`.

 
[remark]:<slide>(new)
### XML konfigurace
```xml
<beans xmlns="http://www.springframework.org/schema/beans"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation="http://www.springframework.org/schema/beans
   http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
  
   <bean id="demoBean" 
         class="jm.spring.cotext.DemoBean" 
         scope="session" />
</beans>
```

[remark]:<slide>(wait)
### Java konfigurace
```java
@Service
@Scope("session")
public class DemoBean
{
   //Some code
}
```

[remark]:<slide>(wait)
#### Příklad: ...

[remark]:<slide>(new)
# Životní cyklus beans
String BeanFactory je zodpovědná za řízení životního cyklu vytvořených bean.

Životní cyklus beans je realizován voláním metod.
- **Post initialization** call back methods
![](media/kpcdR.png)

[remark]:<slide>(new)
- **Pre destruction** call back methods
![](media/kpcdR-2.png)

[remark]:<slide>(wait)
## Definice metod životní cyklu beans
Existují 5 způsoby řízení událostí životního cyklu fazole:

- Implementace rozhraní `InitializingBean` a `DisposableBean`
- *Aware rozhraní pro specifické chování
- Definice metody `init()` a `destroy()` v konfiguračním souboru bean
- Použití annotací `@PostConstruct` a `@PreDestroy`
- Implementaci `BeanPostProcessor`

[remark]:<slide>(new)
### Rozhraní `InitializingBean` a `DisposableBean`
Rozhraní `org.springframework.beans.factory.InitializingBean` umožňuje beaně provést inicializační práci poté, co kontejner nastavil všechny potřebné vlastnosti fazole.

Rozhraní InitializingBean určuje jednu metodu:
```java
void afterPropertiesSet() throws Exception;
```

To není preferovaný způsob, protože těsně spojuje vaši třídu se Spring frameworkem.

Lepším přístupem je použití atributu `init-method` v definici bean.

[remark]:<slide>(wait)
Podobně rozhraní `org.springframework.beans.factory.DisposableBean` umožňuje provést akci, když má být zničena.

Rozhraní DisposableBean určuje jednu metodu:
```java
void destroy() throws Exception;
```

[remark]:<slide>(new)
#### Příklad beanu
```java
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
 
public class DemoBean implements InitializingBean, DisposableBean {
 
    @Override
    public void afterPropertiesSet() throws Exception {
        //Bean initialization code
    }
     
    @Override
    public void destroy() throws Exception {
        //Bean destruction code
    }
}
```


[remark]:<slide>(new)
### *Aware rozhraní
Spring nabízí řadu `*Aware` rozhraní, jejichž implementací říkáme kontejneru, že chceme ragovat ně událost životního cyuklu.

Každé rozhraní bude vyžadovat provedení metody pro zavedení závislosti v bean.

Tato rozhraní lze shrnout takto:

| Aware interface | Purpose |
| :--- | :--- |
| `ApplicationContextAware`  | Interface to be implemented by any object that wishes to be notified of the ApplicationContext that it runs in.
|
| `ApplicationEventPublisherAware` | Set the ApplicationEventPublisher that this object runs in.
|
| `BeanClassLoaderAware` | Callback that supplies the bean class loader to a bean instance.
|
| `BeanFactoryAware` | hrows BeansException;	Callback that supplies the owning factory to a bean instance.
|
| `BeanNameAware` | Set the name of the bean in the bean factory that created this bean.
|
| `BootstrapContextAware` | Set the BootstrapContext that this object runs in.
|
| `LoadTimeWeaverAware` | Set the LoadTimeWeaver of this object’s containing ApplicationContext.
|
| `MessageSourceAware` | Set the MessageSource that this object runs in.
|
| `NotificationPublisherAware` | Set the NotificationPublisher instance for the current managed resource instance.
|
| `PortletConfigAware` | Set the PortletConfig this object runs in.
|
| `PortletContextAware` | Set the PortletContext that this object runs in.
|
| `ResourceLoaderAware` | Set the ResourceLoader that this object runs in.
|
| `ServletConfigAware` | Set the ServletConfig that this object runs in.
|
| `ServletContextAware` | Set the ServletContext that this object runs in.
|


[remark]:<slide>(new)
### Vlastní metody `init()` a `destroy()`
V konfifuračním souboru lze definat metody, které se provlaji v okamžiku vzniku a zrušení bean.

Tyto metody jemožné definovat:
- **Lokálně** v rámci definice bean.
- **Globálně** pro všechny beany v kontextu

[remark]:<slide>(wait)
#### Lokální definice

```xml
<beans>
    <bean id="demoBean" 
          class="jm.spring.code.DemoBean"
          init-method="customInit"
          destroy-method="customDestroy"/>
</beans>
```

[remark]:<slide>(new)
#### Globální definice
Tyto metody jsou vyvolány pro všechny v definičním souboru.

Vyžadují definování společných názvů metod, jako je `init()` a `destroy()`.

 ```xml
<beans default-init-method="customInit" 
       default-destroy-method="customDestroy">  
    
    <bean id="demoBean" 
          class="jm.spring.cotext.DemoBean"/>
 
</beans>
```

[remark]:<slide>(wait)
#### Společná implementace java bean
```java
public class DemoBean {
    public void customInit() {
        System.out.println("Method customInit() invoked...");
    }
 
    public void customDestroy() {
        System.out.println("Method customDestroy() invoked...");
    }
}
```


[remark]:<slide>(new)
### Annotace `@PostConstruct` a `@PreDestroy`
Definované ve verzí Spring Framework 2.5.
 
Označují semetody bez parametru a vetšinou i bez návratové hodnoty.
 
- `@PostConstruct` anotovaná metoda bude vyvolána poté, co byla bean vytvořena s použitím výchozího konstruktoru a těsně před tím, než bude instanci vrácena do žádajícího objektu.

- `@PreDestroy` anotovaná metoda je volána těsně před beans je odstraněn z kontejneru.
 
[remark]:<slide>(wait)
```java
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
 
public class DemoBean {
    @PostConstruct
    public void customInit() {
        System.out.println("Method customInit() invoked...");
    }
     
    @PreDestroy
    public void customDestroy() {
        System.out.println("Method customDestroy() invoked...");
    }
}
```

[remark]:<slide>(new)
## Implementace `BeanPostProcessor` 

BeanPostprocesor umožňuje provádet další kód před a po metodě callback metodě.

Zpracovávat všechny instance beanů v kontejneru IoC jeden po druhém, ne pouze jednu bean instanci.

Typicky se používají pro kontrolu platnosti vlastností beanu nebo jejich změnu.

[remark]:<slide>(wait)
#### Vytvoření třídy BeanPostProcessor
```java
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
 
public class CustomBeanPostProcessor implements BeanPostProcessor {
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println("PostProcessBeforeInitialization() for :" + beanName);
        return bean;
    }
     
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("PostProcessAfterInitialization() for :" + beanName);
        return bean;
    }
}
```

[remark]:<slide>(new)
### Kdy je BeanPostProcessor aktivován

Spring při vytváření beans postupuje následovně:

1.
Vytvoření instance beany buď konstruktorem nebo tovární metodou

2.
Nastavte hodnot a závislostí beany

3.
metod definovaných *Aware rozhraními

4.
Volání metody `postProcessBeforeInitialization()`

5.
Volání callbask metody inicializace

6.
Volání metody `postProcessAfterInitialization()`

7.
Bean je připravena k použití

8.
Při ukončení volaní destroy metod.

[remark]:<slide>(new)
### Jak zaregistrovat BeanPostProcessor

Registrovat v se provádí definici beany.

Tím se BeanPostProcessor **aktivuje automaticky**.

[remark]:<slide>(wait)
#### Příklad: 

```java
public class EmployeeDAOImpl implements EmployeeDAO {
    
    public EmployeeDTO createNewEmployee() {
        EmployeeDTO e = new EmployeeDTO();
        e.setId(1);
        e.setFirstName("Lokesh");
        e.setLastName("Gupta");
        return e;
    }
     
    public void initBean() {
        System.out.println("Init Bean for : EmployeeDAOImpl");
    }
     
    public void destroyBean() {
        System.out.println("Init Bean for : EmployeeDAOImpl");
    }
}
```

[remark]:<slide>(new)
```xml
<bean id="customBeanPostProcessor" 
      class="jm.spring.cotext.CustomBeanPostProcessor" />
     
<bean id="dao" 
      class="jm.spring.cotext.EmployeeDAOImpl"  
      init-method="initBean" 
      destroy-method="destroyBean"/>
```

[remark]:<slide>(wait)
Vytvořní kontextu:
```java
ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
```

[remark]:<slide>(wait)
Výstup na consoli:
```
Called postProcessBeforeInitialization() for : dao
Init Bean for : EmployeeDAOImpl
Called postProcessAfterInitialization() for : dao
```

[remark]:<slide>(new)
# Spring Bean Autowiring

V Spring frameworku je deklarování závislostí bean v konfiguračních souborech základ.

Spring je ale také schopen vytvářet vztahy mezi beany automaticky.

Funkce autowiring má pět režimů:
**no**, **byName**, **byType**, **konstruktor** a **autodetect**.

Výchozí režim je **no**, tedy autowiring je potřeba zapnout konfiguraci.
  
[remark]:<slide>(new)
### Režim: byName 
Tato volba umožňuje nastavit závislostí na základě názvů bean v kontextu.

Při autowiring vlastnosti v bean, název vlastnosti se používá pro vyhledávání odpovídající definice bean v konfiguračním souboru.

Pokud není nalezena žádná taková bean, dojde k chybě.

[remark]:<slide>(wait)
#### Příklad: *konfiruační soubor*
```xml
<context:component-scan base-package="jm.spring.core"/>   
 
<bean id="employee" 
      class="jm.spring.cotext.EmployeeBean" 
      autowire="byName">
    <property name="fullName" 
              value="Lokesh Gupta"/>
</bean>

<bean id="departmentBean" 
      class="jm.spring.cotext.DepartmentBean">
    <property name="name" 
              value="Human Resource"/>
</bean>
```

[remark]:<slide>(new)
#### Příklad: *Impementace beans*
```java
public class EmployeeBean{
    private String fullName;
    private DepartmentBean departmentBean;
    
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
   
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
```

```java
public class DepartmentBean {
    private String name;
 
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
```

[remark]:<slide>(new)
### Režim: byType 
Tato volba umožňuje nastavit závislostí na základě typu beanu.

Při autowiring vlastnosti v bean, se použije typ vlastnosti pro vyhledávání odpovídající definice bean v konfiguračním souboru.

Pokud není nalezena žádná taková bean, dojde k chybě.

[remark]:<slide>(wait)
#### Příklad: *konfiruační soubor*
```xml
<bean id="employee" 
      class="jm.spring.cotext.EmployeeBean" 
      autowire="byType">
    <property name="fullName" 
              value="Lokesh Gupta"/>
</bean>

<bean id="departmen" 
      class="jm.spring.cotext.DepartmentBean">
    <property name="name" 
              value="Human Resource"/>
</bean>
```

[remark]:<slide>(new)
#### Příklad: *Impementace beans*
```java
public class EmployeeBean {
    private String fullName;
    private DepartmentBean departmentBean;
    
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
   
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
```

```java
public class DepartmentBean {
    private String name;
 
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
```

[remark]:<slide>(new)
### Režim: constructor
Tento typ je podobný režimu **byType**.

Jsou detekovány argumenty v konstruktoru.

BeanFactory najde odpopídající beany v kontextu a předá je konstruktoru při vytváření bean.

Pokud v kontextu není právě jedna bean pro každá konstruktor argument, je vyvolána chyba.


[remark]:<slide>(wait)
#### Příklad: *konfiruační soubor*
```xml 
<bean id="employee" 
      class="jm.spring.cotext.EmployeeBean" 
      autowire="constructor">
    <property name="fullName" 
              value="Lokesh Gupta"/>
</bean>

<bean id="departmen" 
      class="jm.spring.cotext.DepartmentBean">
    <property name="name" 
              value="Human Resource"/>
</bean>
```

[remark]:<slide>(new)
#### Příklad: *Impementace beans*
```java
public class EmployeeBean
{
    private String fullName;
      
    private DepartmentBean departmentBean;
    
    public EmployeeBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
}
```

```java
public class DepartmentBean {
    private String name;
 
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
```

[remark]:<slide>(new)
### Režim: autodetect
Autowiring pomocí autodetekce používá oba režimy:
- konstruktoru
- byType.

Nejprve se bude snažit hledat platný konstruktor s argumenty, je-li zvolen režim konstruktoru.

Pokud neexistuje žádný konstruktor definovaný v bean, nebo pokud je k dispozici explicitní konstruktor no-args, je zvolen režim autowire byType.

**Aktuálně stále k dispozici, ale označen za deprecated.**

[remark]:<slide>(new)
## @Autowired annotation
Kromě režimů autowiring poskytovaných v konfiguračním souboru bean lze autowiring zadat ve třídách beanů také pomocí anotace @Autowired.

Chcete-li použít @Autowired anotaci ve třídách bean, musíte nejprve povolit anotaci v konfiguraci.

```xml
<context:annotation-config />
```

nebo definovat bean typu `AutowiredAnnotationBeanPostProcessor`

```xml
<bean class="org...annotation.AutowiredAnnotationBeanPostProcessor"/>
``` 
[remark]:<slide>(new)
### Používání @Autowired annotation
Musí být povolena konfigurace anotací.

Je možné požít annotaci `@Autowired` pro definování závislostí třemi způsoby:
 
[remark]:<slide>(wait)
#### 1) @Autowired na atributu
Když je na atributech použit `@Autowired`, je v konfiguračním souboru ekvivalentní autowiring podle **byType**.

```java
public class EmployeeBean
{
    @Autowired
    private DepartmentBean departmentBean;
 
    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
    //More code
}
```

[remark]:<slide>(new)
#### 2) @Autowired na setteru
Když je na settrech použit parametr `@Autowired`, je v konfiguračním souboru ekvivalentní autowiring podle **byType**.

```java
public class EmployeeBean
{
    private DepartmentBean departmentBean;
 
    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }
 
    @Autowired
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
    //More code
}
```

[remark]:<slide>(new)
#### 3) @Autowired na konstruktoru
Když je na konstruktoru použit parametr `@Autowired`, je v konfiguračním souboru ekvivalentní autowiring podle **constructor**.

```java
public class EmployeeBean
{
    @Autowired
    public EmployeeBean(DepartmentBean departmentBean)
    {
        this.departmentBean = departmentBean;
    }
 
    private DepartmentBean departmentBean;
 
    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
    //More code
}
```

[remark]:<slide>(new)
### Annotace @Qualifier
Autowiring pracuje v režimu „byType“ pro určení závislosti.

Není-li takový typ nalezen, je vyvolána chyba.

Ale co když existují *dvě nebo více* bean pro stejný typ třídy.

v takovém případě musíme BeanFactory napovědět pomocí krantifikátoru.

K tomu učelu slouží annnotace @Qualifier anotaci spolu s @Autowired anotací.

[remark]:<slide>(new)
#### Příklad: Java code
```java
public class EmployeeBean
{
    @Autowired
    @Qualifier("finance")
    private DepartmentBean departmentBean;
 
    public DepartmentBean getDepartmentBean() {
        return departmentBean;
    }
    public void setDepartmentBean(DepartmentBean departmentBean) {
        this.departmentBean = departmentBean;
    }
    //More code
}
```

[remark]:<slide>(new)
#### Příklad: konfigurace
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans>
    <context:annotation-config />
 
    <bean id="employee" 
          class="jm.spring.cotext.EmployeeBean"
          autowire="constructor">
        <property name="fullName" 
                  value="Lokesh Gupta"/>
    </bean>
 
    <!--First bean of type DepartmentBean-->
    <bean id="humanResource" 
          class="jm.spring.cotext.DepartmentBean" >
        <property name="name" 
                  value="Human Resource" />
    </bean>
 
    <!--Second bean of type DepartmentBean-->
     <bean id="finance" 
           class="jm.spring.cotext.DepartmentBean" >
        <property name="name" 
                  value="Finance" />
    </bean>
</beans>
```

[remark]:<slide>(new)
### Annotace @Autowired byName
Spring při definice pomocí atributu třídy používá název atributu také pro dohledání závislosti.

Jako by byla použita annotace @Qualifier("finance")

```java
public class EmployeeBean
{
    @Autowired
    private DepartmentBean finance;
 
    //More code
}
```
[remark]:<slide>(new)
### Paramter annotace @Autowired `required=false`
Spring ve výchozím nastavení považuje všechny vazby za **povinné**.

Pokud není detekovanou vazbu schopen naplni, končí zavádění aplikace s chybou.

Toto je ve většíně případů chtěnné chování, ale ...

Pokud bychom chtěli vazby udělat volitelné, pak můžeme:

[remark]:<slide>(wait)
#### 1) použít parametrt annotace `required = false`

```java
@Autowired (required=false)
@Qualifier ("finance")
private DepartmentBean departmentBean;
```

[remark]:<slide>(wait)
#### 2) Nastavit toto chování globálně
```xml
<bean class="org...annotation.AutowiredAnnotationBeanPostProcessor">
    <property name="requiredParameterValue" value="false" />
</bean>
```

[remark]:<slide>(new)
### Vyřazení beany kandidátů na autowing
Ve výchozím nastavení se pro autowiring hledají včechny beany v kontextu.
 
Spring nabízí možnost některé beany z tohoto vyloučit.

Vyřazení beany ze seznamu kandidátů se provede annotací `autowire-candidate = "false"`

[remark]:<slide>(new)
#### Příklad: *Vyřazení jedné beany*
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans>
    <context:annotation-config />
 
    <bean id="employee" 
          class="jm.spring.cotext.EmployeeBean"
          autowire="constructor">
        <property name="fullName" 
                  value="Lokesh Gupta"/>
    </bean>
    
    <!--Will be available for autowiring-->
    <bean id="humanResource" 
          class="jm.spring.cotext.DepartmentBean" >
        <property name="name" 
                  value="Human Resource" />
    </bean>
 
    <!--Will not participate in autowiring-->
     <bean id="finance"      
           class="jm.spring.cotext.DepartmentBean" 
           autowire-candidate="false">
        <property name="name" 
                  value="Finance" />
    </bean>
</beans>
```

[remark]:<slide>(new)
### Autowired pomocí vzoru
Další možností je omezit kandidáty na autowire na základě vzorů podle názbu beany.

Ktomu slouží atribute `default-autowire-clients` na kořenovém elementu.

[remark]:<slide>(new)
#### Příklad: *filtrace podle jmen*
Chceme použít jen ty beanym s názem konřícím na **Impl** a **Dao**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans default-autowire-candidates="*Impl,*Dao">
    <context:annotation-config />
 
    <bean id="employee" 
          class="jm.spring.cotext.EmployeeBean"
          autowire="constructor">
        <property name="fullName" 
                  value="Lokesh Gupta"/>
    </bean>
     
    <!--Will not participate in autowiring-->
     <bean id="financeImpl"      
           class="jm.spring.cotext.DepartmentBean" 
           autowire-candidate="false">
        <property name="name" 
                  value="Finance" />
    </bean>
</beans>
```

Annotace `autowire-candidate` má vždy přednost před defincí pomocí vzoru.


[remark]:<slide>(new)
# Spring @Component
Autowiring pomocí annnotace @Autowired odpovídá ze vytváření vazeb mezi beany.

Stále musíme definovat beany, aby o nich kontejner věděl a mohl je pro vás zpravovat.

Pro definici beant můžeme použit také annotace:

- `@Component`, 
- `@Repository`, 
- `@Service`a 
- `@Controller`.

Tyto anotace se nazývají také stereotypové anotace.

Dále musíme povolit automatická skenováním komponent.

[remark]:<slide>(new)
### Annotace `@Component`
@Component anotace označí třídu java jako beanu.

Spring ji následně najde pomocí **component-scanning**.
 
Instance beany je pak vytvořena v kontextu.

```java
@Component
public class EmployeeDAOImpl implements EmployeeDAO {
    ...
}
```

[remark]:<slide>(wait)
### Annotace `@Repository`
Rošiřuje použití annotace `@Component`.
 
Je určená specificky pro DAOs.


Kromě importu DAO do kontejneru DI, rovněž tranformuje nekontrolované výjimky na `DataAccessException`.

[remark]:<slide>(new)
### Annotace `@Service`
Anotace @Service je také specializací anotace `@Component`.
V současné době neposkytuje žádné další chování.

Zvušeje ale čitelnost a přehledno kodu.

Podpůrné nástroje a další chování by se na něj mohly v budoucnu přidat.
[remark]:<slide>(wait)
### Annotace `@Controller`  
Anotace `@Controller` označuje třídu jako controller Spring Web MVC.

Je to také specializace `@Component`

Když přidáme anotaci `@Controller` do třídy, můžeme použít anotaci, `@RequestMapping` a mapovat URL na volání metod.

[remark]:<slide>(new)
# Konfigurace component scanning
Aby spring beany definovanté pomocí annotací hedal a našel, musí se mu to explicitne definovat

K tomu je možné použít `context:component-scan` element v applicationContext.xml.

```xml
<context:component-scan base-package="jm.spring.cotext.service" />
<context:component-scan base-package="jm.spring.cotext.dao" />
<context:component-scan base-package="jm.spring.cotext.controller" />
```

Element `context:component-scan` vyžaduje atribut `base-package`, který, určuje výchozí bod pro vyhledávání 

Když je deklarován `component-scan`, nemusíte uvádět `annotation-config` protože je poolen automaticky.

Alternativou k použití xml je použití annotace `@ComponentScan`.


[remark]:<slide>(new)
# Annotace `@Configuration`
Annotace `@Configuration` lze použít ke kenfiguraci aplikace a definici nových bean z kodu.

`@Configuration` označuje, že třída deklaruje jednu nebo více metod bean.

Tato annotace je použítelná od verze 3.

Této funkci se říká Spring Java Config a je "preferovanější" než starší xml konfigurace.

[remark]:<slide>(wait)
#### Použití annotaci `@Configuration`

Metody této třídy označené annotací `@Bean` jsou použity pro generová bean za běhu.

```java
@Configuration
public class AppConfig {
 
    @Bean(name="demoService")
    public DemoClass service() {
        
    }
}
```

[remark]:<slide>(new)
# Zadání
	Tvorime si uvnitr aplikace programaticky dynamicky vlastni spring context – potrebuju aby se s lidma probraly takove ty “interni strivka springu”.
	hierarchie applikacnich kontextu v aplikaci
	Spring expression language – k cemu kdy a proc
•    Spring-context, Spring-beans
	bean definition vs.
bean instance – jake jsou moznosti definovani spring contextu a jak je pouzivat v ramci jednoho kontextu
	jak vypada bean definition a jak ji treba za behu pridat
	jak udelat vlastni springem respektovanou anotaci 
	Jak resit spring missing bean – likely problem in context scanning, nebo missing dependency….
	spring annotation processing – proc a jak spring pouziva proxy a co to je…
	Application context vs.
BeanFactory – rozdily a proc to neni jedna classa
	@Service vs.
@Component vs.
@Bean vs.
@Configuration
  Jak udelat nejakou vlastni AutoConfigure @EnableSomething anotaci – viz.
@Import
Annotace + annotation procesing + vlastní spring annotace + Java reflectio + AOP

