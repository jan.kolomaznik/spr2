[remark]:<class>(center, middle)
# Spring Expression Language
## SpEl

[remark]:<slide>(new)
## Co to je SpEL
SpEL je jazyk integorvaný do Spring od verze 3.0.

Je podobný jako EL z JSP a Unified EL.

Priární učel ve Spring na konfigurace beans. 

SpEL dovoluje:
- praovat s fukcemi a metodami,
- dotazovat se,
- manipulovat s objekty.

Některé funkce podporované výrazovým jazykem jsou následující:
- Literal expressions
- Boolean and relational operators
- Regular expression
- Accessing properties, arrays, lists and maps
- Bean references
- Method invocation
- User-defined functions
- Variables

[remark]:<slide>(new)
## Fungovaní SpEL
![](media/spel-api.png)

[remark]:<slide>(new)
### Příklad: *Hello SpEL*
V tomto příkladu vytvoříte objekt `ExpressionParser`. 

Pomocí toho vytvoříte instanci výrazu a zadáte zprávu `Hello SPEL`. 

Zpráva se zobrazí pomocí objektu ExpressionParser.

```java
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

public class TestSpEL {
    public static void main(String[] args) {
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression("'Hello SPEL'");
        String message = (String) exp.getValue();
        System.out.println(message);
    }
}
```

[remark]:<slide>(new)
## Operace

| Type | Operators |
| :--- | :--- |
| Arithmetic | `+, -, *, /, %, ^, div, mod` |
| Relational | `<, >, ==, !=, <=, >=, lt, gt, eq, ne, le, ge` |
| Logical | `and, or, not, &&, !` |
| Conditional | `?:` |
| Regex | `matches` |

[remark]:<slide>(new)
## Použití v `@Value`
Výrazy SpEL začínají symbolem `#` a jsou zabaleny do závorek: `#{expression}`.

Properties lze odkazovat podobným způsobem, počínaje symbolem `$` a zabalené v závorkách: `${property.name}`.

Properties nemohou obsahovat výrazy SpEL, ale SpEL může obsahovat odkazy na properties:

```java
@Value("#{${someProperty} + 2}")
private int add;
```

Ve výše uvedeném příkladu předpokládejme, že parametr someProperty má hodnotu 2, takže výsledný výraz by byl 2 + 2, který by byl vyhodnocen na hodnotu 4.

[remark]:<slide>(wait)
```java
@Value("#{${someProperty} == 2}")
private boolean compare;
```

[remark]:<slide>(new)
### Podmínky
Podmíněné operátory se používají pro injekci různých hodnot v závislosti na některých podmínkách:

```java
@Value ("# {2> 1? 'A': 'b'}") // "a"
private String ternary;
```

[remark]:<slide>(wait)
Dalším běžným použitím pro ternární operátor je zkontrolovat, zda je některá proměnná `null` a poté vrátit hodnotu proměnné nebo výchozí hodnotu:

```java
@Value ("# {someBean.someProperty! = Null? SomeBean.someProperty: 'default'}")
private String ternary;
```

[remark]:<slide>(wait)
Operátor `Elvis` je způsob zkrácení syntaxe ternárního operátoru.

```java
@Value ("# {someBean.someProperty?: 'Default'}")
private String elvis;
```
[remark]:<slide>(new)
### Regurární výraz
Operátor `matches` lze použít ke kontrole, zda řetězec odpovídá danému regulárnímu výrazu.

```java
@Value("#{'100' matches '\\d+' }")
private boolean validNumericStringResult;
 
@Value("#{'100fghdjf' matches '\\d+' }")
private boolean invalidNumericStringResult;
 
@Value("#{'valid alphabetic string' matches '[a-zA-Z\\s]+' }")
private boolean validAlphabeticStringResult;
 
@Value("#{'invalid alphabetic string #$1' matches '[a-zA-Z\\s]+' }")
private boolean invalidAlphabeticStringResult;
 
@Value("#{someBean.someValue matches '\d+'}")
private boolean validNumericValue;
```

[remark]:<slide>(new)
### Práce s Listem nebo polem
S pomocí SpEL můžeme přistupovat k obsahu pole v kontextu.

Mějme beanu obsahující jména.

```java
@Component("workersHolder")
public class WorkersHolder {
    private List<String> workers = new LinkedList<>();
 
    public WorkersHolder() {
        workers.add("John");
        workers.add("Susie");
        workers.add("Alex");
        workers.add("George");
    }
 
    //Getters and setters
}
```

v SpEL ji pak můžeme použit následovně:

```java
@Value("#{workersHolder.workers[0]}") // John
private String firstWorker;
 
@Value("#{workersHolder.workers.size()}") // 4
private Integer numberOfWorkers;
```

[remark]:<slide>(new)
### Inline list
Lisy můžete také přímo vytvořit přímo výrazem `{}`. 

```java
// evaluates to a Java list containing the four numbers
List numbers = (List) parser.parseExpression("{1,2,3,4}")
                            .getValue(context);

List listOfLists = (List) parser.parseExpression("{{'a','b'},{'x','y'}}")
                                .getValue(context);
```

[remark]:<slide>(new)
### Inline pole
Pole můžete také přímo vytvořit přímo pomocí syntaxw blízké jazyku Java. 

```java
int[] numbers1 = (int[]) parser.parseExpression("new int[4]")
                               .getValue(context);

// Array with initializer
int[] numbers2 = (int[]) parser.parseExpression("new int[]{1,2,3}")
                               .getValue(context);

// Multi dimensional array
int[][] numbers3 = (int[][]) parser.parseExpression("new int[4][5]")
                                   .getValue(context);
```
[remark]:<slide>(new)
### Práce s mapou
S pomocí SpEL můžeme přistupovat také k obsahu mapy.

Mějme beanu obsahující platy.

```java
public class WorkersHolder {
    private Map<String, Integer> salaryByWorkers = new HashMap<>();
 
    public WorkersHolder() { 
        salaryByWorkers.put("John", 35000);
        salaryByWorkers.put("Susie", 47000);
        salaryByWorkers.put("Alex", 12000);
        salaryByWorkers.put("George", 14000);
    }
 
    //Getters and setters
}
```

v SpEL ji pak můžeme použit následovně:

```java
@Value("#{workersHolder.salaryByWorkers['John']}") // 35000
private Integer johnSalary;
```

[remark]:<slide>(new)
### Inline map
Mapy můžete také přímo vytvořit přímo výrazem `{key: value}`. 

```java
// evaluates to a Java map containing the two entries
Map inventorInfo = (Map) parser
        .parseExpression("{name:'Nikola',dob:'10-July-1856'}")
        .getValue(context);

Map mapOfMaps = (Map) parser
        .parseExpression("{name:{first:'Nikola',last:'Tesla'},dob:{day:10,month:'July',year:1856}}")
        .getValue(context);
```